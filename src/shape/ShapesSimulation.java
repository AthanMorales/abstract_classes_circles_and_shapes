/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shape;

/**
 *
 * @author Mauricio
 */
public class ShapesSimulation {

    public static void main(String[] args) {
        Shape s1 = new Circle("Red", 8);
        Shape s2 = new Square("Aqua", 6);

        System.out.println(s1.toString());
        System.out.println(s2.toString());
    }
}
