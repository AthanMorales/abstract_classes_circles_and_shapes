/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shape;

/**
 *
 * @author Mauricio
 */
public class Square extends Shape {

    double length;

    public Square(String color, double length) {
        super(color);
        System.out.println("Square constructor called");
        this.length = length;
    }

    @Override
    double area() {
        return length * 2;
    }
    
    @Override
    double perimeter() {
        return 4 * length ;
    }
    
    @Override
    public String toString() {
        return "Square color is " + super.getColor() + " with area is: " + area() +" and perimeter: " + perimeter();
    }
}
